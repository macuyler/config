# CR-48 Config

This directory contains some scripts and config files that I use on my _CR-48 Chromebook_.

## What's Here?

### Scripts

 - **brightness:** Adjust the screen backlight brightness.
 - **hello:** Connect to a _DHCP_ server and start _X11_.
 - **reverse_scroll:** Reverse the scroll direction of the trackpad.
 - **volume:** Adjust the volume through _Pulse Audio_.

### Config Files

 - **xbindkeys:** Map function keys to media scripts.
 
## About CR-48

The _CR-48 Chromebook_ was a prototype notebook built as a way to test Google's new _ChromeOS_ on example hardware.
It was never meant to be a retail product, so it was only ever given to Google developers.

I got my hands on one of these a couple months ago, and despite its missing battery it booted up just fine!
I was able to update to the latest version of _ChromeOS_, and it was mostly usable.
After about an hour of playing with it, I got bored of _ChromeOS_ and decided to install _Linux_ on it.

Thanks in large part to this _Medium_ article: [Revive your CR-48](https://medium.com/@capuccino/revive-your-cr-48-install-any-os-on-the-fabled-chromebook-b17a763654d9); I was able to get _Debian_ installed and running great!
I opted to use [BSPWM](https://github.com/baskerville/bspwm) as a light weight alternative to a full desktop environment.
This resulted in much better performance, and gave me a lot more room to play with on my _16GB SSD_.
But switching to a tiling window manager (_BSPWM_) did come with some drawbacks.
Most annoying of which was not being able to use the media keys to control things like screen brightness and volume.
So I ended up writing the scripts and config files in this directory to fill in that missing functionality.

Now my _CR-48_ is totally usable for any light work or web browsing that I might want to throw at it.
And even though I won't be using it for any serious work any time soon, it was still a fun project to work on.

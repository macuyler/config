#!/usr/bin/env sh

USERNAME="dev"
CONFIG_URL="https://files.macuyler.com/bin/config.sh"

nopasswd() {
    if [ -z "$(grep $USERNAME /etc/sudoers)" ]; then
        echo "$USERNAME ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
    fi
}

passwd() {
    if [ -n "$(grep $USERNAME /etc/sudoers)" ]; then
        sed -i '$ d' /etc/sudoers
    fi
}

# Run as root please...
apt update && apt install -y sudo curl software-properties-common && \
add-apt-repository -y universe && \
useradd -m -s /bin/bash -G sudo $USERNAME || \
    exit 1

nopasswd
sudo -u $USERNAME curl -L -o /home/$USERNAME/config.sh $CONFIG_URL && \
    sudo -u $USERNAME bash /home/$USERNAME/config.sh && \
        sudo rm /home/$USERNAME/config.sh
passwd

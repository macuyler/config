#!/usr/bin/env sh

SCRIPT=$1
TARGET=$2
SUITE="noble"
MIRROR="http://archive.ubuntu.com/ubuntu/"
CONFIG_SCRIPT="$HOME/Downloads/devroot/devconfig.sh"
SUDO="sudo"

bootstrap() {
    # Check for debootstrap
    DEBIANISH=$(grep debian < /etc/os-release)
    if [ -n "$DEBIANISH" ]; then
        "$SUDO" apt update && "$SUDO" apt install -y debootstrap
    elif [ -n "$(command -v debootstrap)" ]; then
        echo "You must install debootstrap to run this script."
        echo "* https://wiki.debian.org/Debootstrap"
        exit 1
    fi
    # Run deboostrap
    "$SUDO" debootstrap "$SUITE" "$TARGET" "$MIRROR"
}

mount() {
    # Mount system directories
    "$SUDO" mount -t proc /proc "$TARGET/proc" && \
    "$SUDO" mount --rbind /sys "$TARGET/sys" && \
    "$SUDO" mount --rbind /dev "$TARGET/dev" && \
    "$SUDO" mount --rbind /dev/pts "$TARGET/dev/pts"
}

umount() {
    # Unmount system directories
    "$SUDO" umount \
        "$TARGET/proc" \
        "$TARGET/sys" \
        "$TARGET/dev" \
        "$TARGET/dev/pts"
}

configure() {
    # Copy configuration script  to target
    "$SUDO" cp "$CONFIG_SCRIPT" "$TARGET/config.sh"
    "$SUDO" chmod +r "$TARGET/config.sh"
    # Run configuration script
    "$SUDO" chroot "$TARGET" bash /config.sh
    # Cleanup
    "$SUDO" rm "$TARGET/config.sh"
}

# Execute script
$SCRIPT

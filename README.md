## Setup

```bash
git clone https://gitlab.com/macuyler/config.git ~/.local/config
~/.local/config/bin/config-my
```

> If you choose to clone into a different directory, just set the path to this
> repo as `MY_CONFIG` in your env.

## Utils

* **alert**: Plays a notification sound
* **battery**: Battery percentage for tmux status
* **brightness**: Simple brightnessctl wrapper with notifications
* **config-my**: Copies config files to your home directory
* **devsync**: Rsync wrapper that ignores developer junk
* **ffmpeg-vaapi**: Run an ffmpeg file conversion using vaapi acceleration
* **format-branch**: Prepend a branch name with @
* **homeas**: Run a program with a specific `HOME` and `XDG_DATA_HOME` value
* **install-my**: Install groups of common packages and utilities
* **invert**: Invert the colors of your X11 display
* **low-power**: Send a low battery notification
* **microphone**: Toggle the mute state of the current microphone
* **monitor**: Configure a second monitor for BSPWM
* **sandbox**: Quickly open up hidden dir to mess around in
* **search**: Quick web searches using lynx
* **setup-vim**: Install all plugins used in the vimrc
* **sudossh**: Run an ssh command with `sudo` on the remote machine
* **tm**: Create or attach to a tmux session for your CWD
* **upsidedown**: Flip the screen upside down
* **volume**: A pulsemixer wrapper with notifications
* **xsudo**: Escalate privileges by way of the sudo command
* **x11sh**: Spawn a machinectl shell for the current X11 display

#!/usr/bin/env sh

killall -q polybar && \
  while pgrep -u "$USER" -x polybar >/dev/null; do sleep 1; done

# Main Monitor
MAIN_MONITOR=$(cat ~/.mainmonitor)
MONITOR="$MAIN_MONITOR" polybar --reload macuyler &

# Other Monitors
if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    if [ "$m" != "$MAIN_MONITOR" ]; then
      MONITOR=$m polybar --reload friend &
    fi
  done
fi

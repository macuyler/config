ghc_cmds=(ghc ghci stack cabal ghcup)

lazy_ghcup() {
  unset -f $ghc_cmds

  [ -f "$HOME/.ghcup/env" ] && source "$HOME/.ghcup/env"
}

setup lazy_ghcup $ghc_cmds

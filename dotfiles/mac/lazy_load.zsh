setup() {
  local callback=$1
  shift 1
  for cmd in $@
  do
    eval "$cmd() { $callback; $cmd "'$@'"; }"
  done
}

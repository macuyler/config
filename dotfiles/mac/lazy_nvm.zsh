nvm_cmds=(nvm node npm npx firebase uvcc tsserver flutterfire)

lazy_nvm() {
  unset -f $nvm_cmds

  export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"

  brewed_nvm=0
  brew ls --versions nvm > /dev/null 2> /dev/null && \
    brewed_nvm=1

  if [ "$brewed_nvm" -eq "1" ]; then
    . "$(brew --prefix nvm)/nvm.sh"
    . "$(brew --prefix nvm)/etc/bash_completion.d/nvm"
  else
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
  fi
}

setup lazy_nvm $nvm_cmds

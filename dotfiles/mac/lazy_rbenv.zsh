rbenv_cmds=(rbenv ruby jekyll bundle flutter)

lazy_rbenv() {
  unset -f $rbenv_cmds
  export PATH="$HOME/.rbenv/bin:$PATH"
  eval "$(rbenv init -)"
}

setup lazy_rbenv $rbenv_cmds

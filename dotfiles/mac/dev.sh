source ~/.lazy_load.zsh
source ~/.lazy_nvm.zsh
source ~/.lazy_jenv.zsh
source ~/.lazy_rbenv.zsh
source ~/.lazy_ghcup.zsh

# Flutter
export CHROME_EXECUTABLE="/Applications/Chromium.app/Contents/MacOS/Chromium"
export FLUTTER_PATH="$HOME/Library/flutter"
export PATH="$PATH:${FLUTTER_PATH}/bin/cache/dart-sdk/bin"
export PATH="$PATH:${FLUTTER_PATH}/.pub-cache/bin"
export PATH="$PATH":"$HOME/.pub-cache/bin"
export PATH="$PATH:${FLUTTER_PATH}/bin"

# Misc.
export PATH="$PATH:${HOME}/.ebcli-virtual-env/executables"
export PATH="$PATH:/usr/local/opt/mysql-client/bin"
export PATH="$PATH:${HOME}/Library/Python/3.9/bin"
export PATH="$PATH:$HOME/.cargo/bin"
export PATH="$PATH:${HOME}/Library/Android/sdk/platform-tools"

# Shortcuts
alias dc="docker compose"
json() {
	jq --color-output . "$@" | less -R
}
urld() {
	echo -e "$(sed 's/+/ /g;s/%\(..\)/\\x\1/g;')"
}

# Keychain
SSH_KEY="id_ed25519"
alias keyup="keychain --timeout 240 $SSH_KEY"
[ -x "$(command -v keychain)" ] && \
	eval "$(keychain -q --timeout 240 --eval)"

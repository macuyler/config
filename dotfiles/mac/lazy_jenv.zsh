jenv_cmds=(jenv java ghidraRun)

lazy_jenv() {
  unset -f $jenv_cmds

  [ -f /usr/libexec/java_home ] && \
    export JAVA_HOME=$(/usr/libexec/java_home)

  export PATH="$HOME/.jenv/bin:$PATH"
  eval "$(jenv init -)"
}

setup lazy_jenv $jenv_cmds

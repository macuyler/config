umask 067
export PATH="$PATH:${HOME}/.local/bin"
export EDITOR="/usr/bin/vi"
export VISUAL="$EDITOR"
source ~/.dev.sh
source ~/.system.sh
alias ls="ls --color=auto"
alias ll="ls -l"
alias la="ls -la"
alias cl="clear"
alias tma="tmux attach || tmux new -s '?'"
alias tmr="tmux -f ~/.tmux.remote.conf"
alias tmk="tmux kill-server"
alias sandbox="source sandbox"
alias '..'="cd .."
alias '...'="cd ../.."
alias '?'="search"
[ ! -d "$HOME/.sandbox" ] && mkdir ~/.sandbox
[ -f ~/.custom.sh ] && source ~/.custom.sh
git_prompt() {
  [ -x "$(command -v format-branch)" ] && format-branch || echo ""
}

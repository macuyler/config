if !empty(globpath(&rtp, 'autoload/plug.vim'))

    " :PlugInstall ~ to install all listed Plugins
    call plug#begin('~/.vim/plugged')
    Plug 'macuyler/gruvbox' " Color Scheme
    Plug 'sheerun/vim-polyglot' " Syntax Highlighting
    Plug 'dense-analysis/ale', { 'on': 'ALEEnable' } " Linting/Formatting/LSP
    call plug#end()

    " Gruvbox
    colorscheme gruvbox
    let g:gruvbox_sign_column = 'dark0'
    let g:gruvbox_invert_selection = 0
    set background=dark

    " ALE
    set omnifunc=ale#completion#OmniFunc
    let g:ale_linter_aliases = {
    \   'jsx': ['javascript'],
    \   'tsx': ['typescript'],
    \   'vue': ['javascript'] }
    let g:ale_fixers = {
    \   '*': ['remove_trailing_lines', 'trim_whitespace'],
    \   'cpp': ['clang-format', 'clangtidy'],
    \   'dart': ['dart-format'],
    \   'go': ['gofmt', 'goimports', 'golines', 'gopls'],
    \   'haskell': ['hindent', 'hlint', 'stylish-haskell'],
    \   'javascript': ['prettier', 'eslint'],
    \   'typescript': ['prettier', 'eslint'],
    \   'php': ['phpcbf'],
    \   'python': ['black', 'isort'],
    \   'rust': ['rustfmt'] }
    let g:ale_fix_on_save = 0
    let g:ale_set_highlights = 0
    let g:ale_set_signs = 0

    " ALE Keybinds
    nnoremap <leader>[ :ALEEnable<CR>
    nnoremap <leader>] :ALEDisable<CR>
    nnoremap <leader>gd :ALEGoToDefinition<CR>
    nnoremap <leader>gt :ALEGoToTypeDefinition<CR>
    nnoremap <leader>gi :ALEGoToImplementation<CR>
    nnoremap <leader>gr :ALEFindReferences<CR>
    nnoremap <leader>w :ALENextWrap<CR>
    nnoremap <leader>b :ALEPreviousWrap<CR>
    nnoremap <leader>e :ALEPopulateQuickfix<CR>
    nnoremap <leader>E :ALEPopulateLocList<CR>
    nnoremap <leader>f :ALEFix<CR>
    nnoremap <leader>r :ALERename<CR>
    nnoremap <leader>K :ALEHover<CR>
    nnoremap <leader>k :ALEDetail<CR>
    nnoremap <leader>? :ALEInfo<CR>

endif

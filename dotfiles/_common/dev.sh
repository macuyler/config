# Android
export ANDROID_SDK_ROOT="$HOME/Library/Android/Sdk"
export ANDROID_NDK_ROOT="$ANDROID_SDK_ROOT/ndk/26.1.10909125"
export ANDROID_HOME="$ANDROID_SDK_ROOT"
export PATH="$PATH:$ANDROID_SDK_ROOT/platform-tools"

# C/C++
export CPLUS_INCLUDE_PATH="/usr/include/c++/11:/usr/include/x86_64-linux-gnu/c++/11"
export ACLOCAL_PATH="/usr/share/aclocal"

# Flutter
export CHROME_EXECUTABLE="$HOME/.local/bin/chrome"
export PATH="$PATH:$HOME/Library/flutter/.pub-cache/bin"
export PATH="$PATH:$HOME/Library/flutter/bin"

# Golang
export GOPATH="${HOME}/.go"
export PATH="$PATH:${GOPATH}/bin"
export PATH="$PATH:/usr/local/go/bin"

[ -d "${GOPATH}" ] || mkdir "${GOPATH}"
[ -d "${GOPATH}/src/github.com" ] || \
  mkdir -p "${GOPATH}/src/github.com"

# Haskell
[ -f "$HOME/.ghcup/env" ] && source "$HOME/.ghcup/env"

# Java
export _JAVA_AWT_WM_NONREPARENTING=1
export PATH="$HOME/.jenv/bin:$PATH"
[ -d "$HOME/.jenv" ] && eval "$(jenv init -)"

# Node.js
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && source "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && source "$NVM_DIR/bash_completion"

# PHP
export PATH="$PATH:$HOME/.config/composer/vendor/bin"
alias artisan="docker compose exec laravel bash"

# Rust
[ -f "$HOME/.cargo/env" ] && source "$HOME/.cargo/env"

# Shortcuts
alias dc="docker compose"
json() {
  jq --color-output . "$@" | less -R
}
urld() {
  echo -e "$(sed 's/+/ /g;s/%\(..\)/\\x\1/g;')"
}

# Keychain
SSH_KEY="id_ed25519_sk"
alias keyup="keychain --timeout 240 $SSH_KEY"
[ -x "$(command -v keychain)" ] && \
  eval "$(keychain -q --timeout 240 --eval)"

vim.o.background = 'dark'

-- Set custom background color
require('gruvbox').setup({
    palette_overrides = {
        dark0 = '#1d1d1d',
    },
    overrides = {
        SignColumn = { bg = '#1d1d1d' },
        GruvboxRedSign = { bg = '#1d1d1d' },
        GruvboxGreenSign = { bg = '#1d1d1d' },
        GruvboxYellowSign = { bg = '#1d1d1d' },
        GruvboxBlueSign = { bg = '#1d1d1d' },
        GruvboxPurpleSign = { bg = '#1d1d1d' },
        GruvboxAquaSign = { bg = '#1d1d1d' },
        GruvboxOrangeSign = { bg = '#1d1d1d' },
    },
})

vim.cmd('colorscheme gruvbox')

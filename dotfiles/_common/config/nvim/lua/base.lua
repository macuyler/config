-- Vim options
local set = vim.opt
set.expandtab = true
set.guicursor = ''
set.modelines = 0
set.number = true
set.path = set.path + '**'
set.relativenumber = true
set.scrolloff = 6
set.shiftwidth = 4
set.showtabline = 0
set.smartindent = true
set.spell = true
set.spelllang = 'en_us'
set.splitbelow = true
set.splitright = true
set.ttimeoutlen = 0
set.wildmode = 'list:longest'
set.wildignore = set.wildignore + {
    '*/node_modules/*',
    '*/build/*',
    '*/dist/*',
    '*/__pycache__/*',
    '*/.venv/*',
    '*/vendor/*',
}

-- Fix netrw
vim.g['netrw_banner'] = 0

-- Leader setup
vim.api.nvim_set_keymap('n', '<BS>', '<leader>', {})
vim.api.nvim_set_keymap('n', '<SPACE>', '<leader>', {})

-- Close list key map
vim.keymap.set('n', '<leader>gc', ':cclose<CR>')
vim.keymap.set('n', '<leader>gl', ':lclose<CR>')

-- Shift width key map
vim.keymap.set('n', '<leader>sw', ':set sw=4<CR>')
vim.keymap.set('n', '<leader>sW', ':set sw=2<CR>')

-- Text width key map
vim.keymap.set('n', '<leader>tw', ':set tw=80<CR>')
vim.keymap.set('n', '<leader>tW', ':set tw=0<CR>')

-- Left padding key map
vim.keymap.set('n', '<leader>z', ':to vne|vert res 50|winc p<CR>')

-- Right padding key map
vim.keymap.set('n', '<leader>x', ':vne|vert res 50|winc p<CR>')

-- Hijack C-g
vim.keymap.set('n', '<C-g>', '<C-b>')
vim.keymap.set('n', '<C-b>', '<C-g>')

-- Command mode key map
vim.keymap.set('c', '<C-_>', '<C-R>=getcwd()<CR>/')

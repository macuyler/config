-- Setup LSP servers
local lsp = require('lspconfig')
lsp.ccls.setup {}
lsp.dartls.setup {}
lsp.gopls.setup {}
lsp.hls.setup {}
lsp.phpactor.setup {}
lsp.pyright.setup {}
lsp.rust_analyzer.setup {}
lsp.ts_ls.setup {}
lsp.zls.setup {}

-- Diagnostic config
vim.diagnostic.config({
    signs = false,
    underline = false,
    virtual_text = false,
})

-- Diagnostic key map
vim.keymap.set('n', '<leader>k', vim.diagnostic.open_float)
vim.keymap.set('n', '<leader>e', vim.diagnostic.setqflist)
vim.keymap.set('n', '<leader>E', vim.diagnostic.setloclist)
vim.keymap.set('n', '<leader>w', vim.diagnostic.goto_next)
vim.keymap.set('n', '<leader>b', vim.diagnostic.goto_prev)

-- LSP key map
vim.api.nvim_create_autocmd('LspAttach', {
    group = vim.api.nvim_create_augroup('UserLspConfig', {}),
    callback = function(ev)
        -- Enable completion triggered by <c-x><c-o>
        vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

        local opts = { buffer = ev.buf }
        vim.keymap.set('n', '<leader>[', ':LspStart<CR>', opts)
        vim.keymap.set('n', '<leader>]', ':LspStop<CR>', opts)
        vim.keymap.set('n', '<leader>?', ':LspStatus<CR>', opts)
        vim.keymap.set('n', '<leader>gd', vim.lsp.buf.definition, opts)
        vim.keymap.set('n', '<leader>gt', vim.lsp.buf.type_definition, opts)
        vim.keymap.set('n', '<leader>gi', vim.lsp.buf.implementation, opts)
        vim.keymap.set('n', '<leader>gr', vim.lsp.buf.references, opts)
        vim.keymap.set('n', '<leader>f', vim.lsp.buf.format, opts)
        vim.keymap.set('n', '<leader>r', vim.lsp.buf.rename, opts)
        vim.keymap.set('n', '<leader>K', vim.lsp.buf.hover, opts)
    end,
})

#!/usr/bin/env sh

# =============================================== #
#                                                 #
# This script will display your laptops current   #
# battery percentage for use in your tmux status. #
#                                                 #
# Usage:                                          #
#  The full tmux battery display.                 #
#    $ battery                                    #
#  Just the battery percentage.                   #
#    $ battery %                                  #
#  Just the battery status.                       #
#    $ battery @                                  #
# =============================================== #


UNAME=$(uname -s)
BAT_PATH=/sys/class/power_supply/BAT0
CHARGING=1
BATTERY=0


output() {
  pct=$1
  status=$2
  arg=$3
  if [ "$arg" = "%" ]; then
    echo "$pct%"
  elif [ "$arg" = "@" ]; then
    [ "$status" = "$CHARGING" ] && echo "Charging"
    [ "$status" = "$BATTERY" ] && echo "Not Charging"
  else
    [ "$status" = "$CHARGING" ] && echo "#[fg=cyan]ꜛ$pct%"
    [ "$status" = "$BATTERY" ] && echo "$pct%"
  fi
}

get_linux_pct() {
  charge_now="$BAT_PATH/charge_now"
  charge_full="$BAT_PATH/charge_full"
  energy_now="$BAT_PATH/energy_now"
  energy_full="$BAT_PATH/energy_full"
  now="0"
  full="1"
  if [ -f "$charge_full" ] && [ -f "$charge_now" ]; then
    now=$(cat $charge_now)
    full=$(cat $charge_full)
  elif [ -f "$energy_full" ] && [ -f "$energy_now" ]; then
    now=$(cat $energy_now)
    full=$(cat $energy_full)
  fi
  echo "$((100 * now / full))"
}

get_linux_battery() {
  pct=$(get_linux_pct)
  status_txt=$(cat $BAT_PATH/status)
  [ "$status_txt" = "Full" ] || \
    [ "$status_txt" = "Charging" ] && status="$CHARGING"
  [ "$status_txt" = "Discharging" ] || \
    [ "$status_txt" = "Not charging" ] && status="$BATTERY"
  output "$pct" "$status" "$1"
}


get_mac_battery() {
  BAT=$(pmset -g batt)
  status_line=$(echo "$BAT" | head -n 1)
  pct_line=$(echo "$BAT" | tail -n 1)
  pct=$(echo "$pct_line" | cut -d'%' -f1 | rev | cut -f1 | rev)
  if echo "$status_line" | grep -q "AC Power"; then
      status="$CHARGING"
  fi
  if echo "$status_line" | grep -c "Battery Power"; then
      status="$BATTERY"
  fi
  output "$pct" "$status" "$1"
}


if [ "$UNAME" = "Linux" ]; then
  [ -d $BAT_PATH ] && \
    get_linux_battery "$1"
elif [ "$UNAME" = "Darwin" ]; then
  get_mac_battery "$1"
fi

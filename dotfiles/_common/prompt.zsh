# ============================================================= #
# This script will configure a custom prompt for you. It allows #
# you to toggle on and off the user, host, and directory. You   #
# can also set a color for each one.                            #
#                                                               #
# Options:                                                      #
#   -u:color   ~ Display the user.                              #
#   -h:color   ~ Display the hostname.                          #
#   -d:color   ~ Display the directory.                         #
#   -db:color  ~ Display the directory and branch.              #
#                                                               #
# Examples:                                                     #
#  1. (blue host, cyan dir and git):                            #
#    % source ~/.prompt.zsh -h:blue -db:cyan                    #
#  2. (magenta user, blue host, green dir):                     #
#    % source ~/.prompt.zsh -u:magenta -h:blue -d:green         #
#                                                               #
# ============================================================= #

DEFAULT_SYMBOL="%#"
COMMAND_SYMBOL="!"

PROMPT_USER_COLOR='white'
PROMPT_HOST_COLOR='white'
PROMPT_DIR_COLOR='white'

SHOW_USER=""
SHOW_HOST=""
SHOW_DIR=""

OPTIONS=("-u" "-h" "-d" "-db")

# Set config from arguments
for arg in "$@"
do
  a1=$(echo $arg | cut -d':' -f1)
  a2=$(echo $arg | cut -d':' -f2)
  if [[ $(printf '%s\n' "${OPTIONS[@]}" | grep -e '^'"$a1"'$') != "" ]]; then
    type="${a1:1:1}"
    # Toggle Prompt Elements
    if [[ "$type" = "u" ]]; then SHOW_USER="$a1" fi
    if [[ "$type" = "h" ]]; then SHOW_HOST="$a1" fi
    if [[ "$type" = "d" ]]; then SHOW_DIR="$a1" fi
    if [[ "$a1" != "$a2" ]]; then
      # Set Colors
      if [[ "$type" = "u" ]]; then PROMPT_USER_COLOR="$a2" fi
      if [[ "$type" = "h" ]]; then PROMPT_HOST_COLOR="$a2" fi
      if [[ "$type" = "d" ]]; then PROMPT_DIR_COLOR="$a2" fi
    fi
  fi
done

dir_prompt() {
  if [[ "$SHOW_DIR" == "-d" ]]; then
    echo " %F{$PROMPT_DIR_COLOR}%1~%f"
  fi
  if [[ "$SHOW_DIR" == "-db" ]]; then
    echo " %F{$PROMPT_DIR_COLOR}%1~$(git_prompt)%f"
  fi
}

host_prompt() {
  if [[ "$SHOW_HOST" == "-h" ]]; then
    echo " %F{$PROMPT_HOST_COLOR}%m%f"
  fi
}

user_prompt() {
  if [[ "$SHOW_USER" == "-u" ]]; then
    echo " %F{$PROMPT_USER_COLOR}%n%f"
  fi
}

symbol_prompt() {
  if [ "${KEYMAP}" = 'vicmd' ]; then
    echo "$COMMAND_SYMBOL"
  else
    echo "$DEFAULT_SYMBOL"
  fi
}

zle-keymap-select() {
  zle reset-prompt
}

setopt prompt_subst
zle -N zle-keymap-select
export PROMPT='$(user_prompt)$(host_prompt)$(dir_prompt) $(symbol_prompt) '
